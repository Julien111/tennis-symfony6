import Vue from 'vue';

Vue.component('titre', {
  
  data: function () {
    return {
      message: "Tennis App"
    }
  },
  template: '<h1 id="title-app"> {{ message }} </h1>'
})

new Vue({ el: '#titre' })