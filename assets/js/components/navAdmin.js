document.addEventListener('DOMContentLoaded', function () {
    //navbar admin

    const menuAdmin = document.querySelector('#menu-admin');
    const burgerAdmin = document.querySelector('#burger-admin');
    const navAdmin = document.querySelector('#nav-admin');
    const main = document.querySelector('main');

    burgerAdmin.addEventListener('click', () => {


        if (menuAdmin.classList.contains('hidden')) {
            menuAdmin.classList.remove('hidden');
            navAdmin.style.height = "170px";
            main.style.paddingTop = "160px";
        }
        else {
            menuAdmin.classList.add('hidden');
            navAdmin.style.height = "50px";
            main.style.paddingTop = "60px";
        }

    });

});