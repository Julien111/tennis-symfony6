//navbar profil

document.addEventListener('DOMContentLoaded', function () {


    const menu = document.querySelector('#menu');
    const burger = document.querySelector('#burger');
    const navProfil = document.querySelector('#navProfil');
    const main = document.querySelector('main');

    burger.addEventListener('click', () => {


        if (menu.classList.contains('hidden')) {
            menu.classList.remove('hidden');
            navProfil.style.height = "170px";
            main.style.paddingTop = "160px";
        }
        else {
            menu.classList.add('hidden');
            navProfil.style.height = "50px";
            main.style.paddingTop = "60px";
        }

    });


});