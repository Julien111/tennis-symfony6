<?php

namespace App\Form;

use App\Entity\Joueur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class JoueurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Le champs ne doit pas être vide',
                ]),
                new Regex(['pattern' => '/[A-Z]{1}[a-z]{0,}/',
                    'message' => "La première lettre du nom doit être en majuscule, y compris pour les noms composés"
                ])
                ],
            ])
            ->add('prenom', TextType::class, [
            'constraints' => [
                new NotBlank([
                    'message' => 'Le champs ne doit pas être vide',
                ]),
                new Regex(['pattern' => '/[A-Z]{1}[a-z]{0,}/',
                    'message' => "La première lettre du prénom doit être en majuscule, y compris pour les prénoms composés"
                ])
                ],
            ])            
            ->add('genre', ChoiceType::class, [
                'choices' => [
                    'Masculin' => 'M',
                    'Féminin' => 'F',                                        
                ],
                'expanded' => true,                
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['cols' => '5', 'rows' => '6'],
                'constraints' => [
                new NotBlank([
                    'message' => 'Le champs ne doit pas être vide',
                ])]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Joueur::class,
        ]);
    }
}