<?php

namespace App\Form;

use App\Entity\Joueur;
use App\Entity\Tournoi;
use App\Entity\MatchChelem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ChelemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('typeTournoi', ChoiceType::class, [
                'choices'  => [
                    'ATP' => 'ATP',
                    'WTA' => 'WTA',                    
                ],
            ])
            ->add('date', DateType::class, [
                'format' => 'dd-MM-yyyy',
            ])
            ->add('tournoi', EntityType::class, ["class" => Tournoi::class, "label" => "Choisir le tournoi : ", "attr" => ["class" => "radio"], 'multiple' => false,
                'expanded' => true,]
            )
            ->add('vainqueur', EntityType::class, ["class" => Joueur::class, "label" => "Choisir le vainqueur du tournoi  : ", "attr" => ["class" => "check"], 'multiple' => false,
                'expanded' => false,])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MatchChelem::class,
        ]);
    }
}