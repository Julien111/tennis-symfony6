<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, ['required' => true, "label" => 'Nom *'])
            ->add('prenom', TextType::class, ['required' => true, "label" => 'Prénom *'])
            ->add('email', EmailType::class, [
                'constraints' => [ new NotBlank([
                    'message' => 'Merci de saisir une adresse email'
                ])],
               'required' => true,
               "label" => 'Courriel *'
            ])           
            ->add('plainPassword', RepeatedType::class, [
                "type" => PasswordType::class,
                'mapped' => false,
                'first_options' => [
                    "label" => 'Mot de passe *'
                ],
                'second_options' => [
                    'label' => "Confirmez le mot de passe *"
                ],
                'invalid_message' => "La confirmation n'est pas similaire au mot de passe.",
                'constraints' => [
                    new NotBlank(),
                    new Length(["min" => 8]),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}