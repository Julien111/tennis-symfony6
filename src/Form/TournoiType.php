<?php

namespace App\Form;

use App\Entity\Tournoi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TournoiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
            'required' => true,
            'constraints' => [new Length(['min' => 3])],
        ])
            ->add('code', TextType::class, [
              'required' => true,
              'constraints' => [
                new Length(['max' => 4]),
                new Regex(['pattern' => '/[A-Z]/',
                    'message' => "Que des lettres mujuscules et maximum 4 caractères."
                ]) 
              ]  
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tournoi::class,
        ]);
    }
}