<?php

namespace App\Form;

use App\Entity\Blogposts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BlogpostsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'constraints' => [
                new NotBlank([
                    'message' => 'Le champs ne doit pas être vide',
                ]),
                new Length(['min' => 3])
            ],
            'required' => true])
            ->add('texte', TextareaType::class, [
                'attr' => ['cols' => '20', 'rows' => '10'],
                'required' => true,
                'constraints' => [
                new NotBlank([
                    'message' => 'Le champs ne doit pas être vide',
                ])],
            ])                     
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Blogposts::class,
        ]);
    }
}