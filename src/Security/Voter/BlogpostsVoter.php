<?php

namespace App\Security\Voter;

use App\Entity\Users;
use App\Entity\Blogposts;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class BlogpostsVoter extends Voter
{
    public const BLOG_EDIT = 'POST_EDIT';
    public const BLOG_VIEW = 'POST_VIEW';

    private $security;

    public function __construct(Security $security){
        $this->security = $security;
    }

    protected function supports(string $attribute, $blog): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::BLOG_EDIT, self::BLOG_VIEW])
            && $blog instanceof \App\Entity\Blogposts;
    }

    protected function voteOnAttribute(string $attribute, $blogPost, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        //On vérifie si le post a un propriétaire

        if(null === $blogPost->getAuteur()){
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::BLOG_EDIT:
                // logic to determine if the user can EDIT
                return $this->canEditBlog($blogPost, $user);
                break;
            case self::BLOG_VIEW:
                // logic to determine if the user can VIEW
                return $this->canDeleteBlog();
                break;
        }

        return false;
    }

    private function canEditBlog (Blogposts $blog, Users $users){
        //Le propriétaire du post peut le modifier
        return $users === $blog->getAuteur();
    }

    private function canDeleteBlog (){
        if($this->security->isGranted('ROLE_EDITOR')){
            return true;
        }
        else{
            return false;
        }
    }
}