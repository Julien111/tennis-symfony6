<?php

namespace App\Entity;

use App\Repository\MatchChelemRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MatchChelemRepository::class)]
class MatchChelem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 3)]
    private $typeTournoi;

    #[ORM\ManyToOne(targetEntity: Tournoi::class, inversedBy: 'matchChelems')]
    #[ORM\JoinColumn(nullable: false)]
    private $tournoi;

    #[ORM\ManyToOne(targetEntity: Joueur::class, inversedBy: 'matchVainqueur')]
    #[ORM\JoinColumn(nullable: false)]
    private $vainqueur;

    #[ORM\Column(type: 'date')]
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeTournoi(): ?string
    {
        return $this->typeTournoi;
    }

    public function setTypeTournoi(string $typeTournoi): self
    {
        $this->typeTournoi = $typeTournoi;

        return $this;
    }

    public function getTournoi(): ?Tournoi
    {
        return $this->tournoi;
    }

    public function setTournoi(?Tournoi $tournoi): self
    {
        $this->tournoi = $tournoi;

        return $this;
    }

    public function getVainqueur(): ?Joueur
    {
        return $this->vainqueur;
    }

    public function setVainqueur(?Joueur $vainqueur): self
    {
        $this->vainqueur = $vainqueur;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}