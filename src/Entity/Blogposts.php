<?php

namespace App\Entity;

use App\Repository\BlogpostsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BlogpostsRepository::class)]
class Blogposts
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $titre;

    #[ORM\Column(type: 'text')]
    private $texte;

    #[ORM\ManyToOne(targetEntity: Users::class, inversedBy: 'blogposts')]
    #[ORM\JoinColumn(nullable: false)]
    private $auteur;

    #[ORM\Column(type: 'date', nullable: true)]
    private $datePost;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getAuteur(): ?Users
    {
        return $this->auteur;
    }

    public function setAuteur(?Users $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDatePost(): ?\DateTimeInterface
    {
        return $this->datePost;
    }

    public function setDatePost(?\DateTimeInterface $datePost): self
    {
        $this->datePost = $datePost;

        return $this;
    }
}
