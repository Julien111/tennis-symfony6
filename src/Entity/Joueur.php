<?php

namespace App\Entity;

use App\Repository\JoueurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JoueurRepository::class)]
class Joueur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'string', length: 255)]
    private $prenom;

    #[ORM\Column(type: 'string', length: 255)]
    private $genre;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\OneToMany(mappedBy: 'vainqueur', targetEntity: MatchChelem::class, orphanRemoval: true)]
    private $matchVainqueur;

    public function __construct()
    {
        $this->matchVainqueur = new ArrayCollection();
    }

    public function __toString() {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, MatchChelem>
     */
    public function getMatchVainqueur(): Collection
    {
        return $this->matchVainqueur;
    }

    public function addMatchVainqueur(MatchChelem $matchVainqueur): self
    {
        if (!$this->matchVainqueur->contains($matchVainqueur)) {
            $this->matchVainqueur[] = $matchVainqueur;
            $matchVainqueur->setVainqueur($this);
        }

        return $this;
    }

    public function removeMatchVainqueur(MatchChelem $matchVainqueur): self
    {
        if ($this->matchVainqueur->removeElement($matchVainqueur)) {
            // set the owning side to null (unless already changed)
            if ($matchVainqueur->getVainqueur() === $this) {
                $matchVainqueur->setVainqueur(null);
            }
        }
        return $this;
    }    
}