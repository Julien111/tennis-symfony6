<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TournoiRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TournoiRepository::class)]
class Tournoi
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'string', length: 4)]
    #[Assert\Length(
        min: 1,
        max: 4,
        minMessage: 'Le code tournoi doit avoir au moins {{ limit }} caractère.',
        maxMessage: 'Le code tournoi ne doit pas dépasser {{ limit }} caractères',
    )]
    private $code;

    #[ORM\OneToMany(mappedBy: 'tournoi', targetEntity: MatchChelem::class, orphanRemoval: true)]
    private $matchChelems;

    public function __construct()
    {
        $this->matchChelems = new ArrayCollection();
    }

    public function __toString() {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection<int, MatchChelem>
     */
    public function getMatchChelems(): Collection
    {
        return $this->matchChelems;
    }

    public function addMatchChelem(MatchChelem $matchChelem): self
    {
        if (!$this->matchChelems->contains($matchChelem)) {
            $this->matchChelems[] = $matchChelem;
            $matchChelem->setTournoi($this);
        }

        return $this;
    }

    public function removeMatchChelem(MatchChelem $matchChelem): self
    {
        if ($this->matchChelems->removeElement($matchChelem)) {
            // set the owning side to null (unless already changed)
            if ($matchChelem->getTournoi() === $this) {
                $matchChelem->setTournoi(null);
            }
        }

        return $this;
    }
}