<?php

namespace App\Repository;

use App\Entity\MatchChelem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchChelem|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchChelem|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchChelem[]    findAll()
 * @method MatchChelem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchChelemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchChelem::class);
    }

    // /**
    //  * @return MatchChelem[] Returns an array of MatchChelem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MatchChelem
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
