<?php

namespace App\Controller\Profil;

use App\Entity\Users;
use App\Form\UsersProfilType;
use App\Form\PasswordFormType;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ProfilController extends AbstractController
{
    #[Route('/profil', name: 'profil')]
    #[IsGranted('ROLE_EDITOR')]
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
            'user' => $user
        ]);
    }

    #[Route('/{id}/edit', name: 'profil_users_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_EDITOR')]
    public function edit(Request $request, Users $user, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UsersProfilType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('profil', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('profil/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/profil/password', name: 'profil_password')]
    #[IsGranted('ROLE_EDITOR')]
    public function profilPassword(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, UsersRepository $usersRepository): Response
    {
        $user = $usersRepository->find($this->getUser());
        
        $form = $this->createForm(PasswordFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //l'ancien password 
            $old_pwd = $form->get('passwordAccount')->getData();
           
            
            if($userPasswordHasher->isPasswordValid($user, $old_pwd)) {
                $newPassword = $form->get('plainPassword')->getData();
                // encode the plain password
                $password = $userPasswordHasher->hashPassword($user, $newPassword);
            
                $user->setPassword($password);            
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'Votre mot de passe a été modifié avec succès !');
            }
            else{
                $this->addFlash('error', 'Il y a une erreur dans votre mot de passe actuel.');
            }
            
            // do anything else you need here, like send an email

            //return new RedirectResponse($this->urlGenerator->generate('profil'));
        }

        return $this->render('profil/password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}