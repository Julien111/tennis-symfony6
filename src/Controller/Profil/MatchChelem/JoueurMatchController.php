<?php

namespace App\Controller\Profil\MatchChelem;

use App\Form\ChelemType;
use App\Entity\MatchChelem;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\MatchChelemRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/profil/match')]
class JoueurMatchController extends AbstractController
{
    #[Route('/', name: 'joueur_match')]
    #[IsGranted('ROLE_EDITOR')]
    public function index(MatchChelemRepository $matchChelemRepository): Response
    {
        return $this->render('joueur_match/index.html.twig', [
            'matchs' => $matchChelemRepository->findAll(),            
        ]);
    }
    
    #[Route('/new', name: 'joueur_match_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_EDITOR')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $match = new MatchChelem();
        $form = $this->createForm(ChelemType::class, $match);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($match);
            $entityManager->flush();

            return $this->redirectToRoute('joueur_match', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('joueur_match/new.html.twig', [
            'match' => $match,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'joueur_match_show', methods: ['GET'])]
    #[IsGranted('ROLE_EDITOR')]
    public function show(MatchChelem $match): Response
    {
        return $this->render('joueur_match/show.html.twig', [
            'match' => $match,
        ]);
    }

    #[Route('/{id}/modifier', name: 'joueur_match_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_EDITOR')]
    public function edit(Request $request, MatchChelem $match, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ChelemType::class, $match);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('joueur_match', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('joueur_match/edit.html.twig', [
            'match' => $match,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'joueur_match_delete', methods: ['POST'])]
    #[IsGranted('ROLE_EDITOR')]
    public function delete(Request $request, MatchChelem $match, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$match->getId(), $request->request->get('_token'))) {
            $entityManager->remove($match);
            $entityManager->flush();
        }

        return $this->redirectToRoute('joueur_match', [], Response::HTTP_SEE_OTHER);
    }
}