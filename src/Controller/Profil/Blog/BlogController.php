<?php

namespace App\Controller\Profil\Blog;

use App\Entity\Blogposts;
use App\Form\BlogpostsType;
use App\Repository\UsersRepository;
use App\Repository\BlogpostsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/profil/blog')]
#[IsGranted('ROLE_AUTHOR')]
#[IsGranted('ROLE_EDITOR')]
class BlogController extends AbstractController
{
    #[Route('/', name: 'blog_index', methods: ['GET'])]
    #[IsGranted('ROLE_AUTHOR')]
    public function index(BlogpostsRepository $blogpostsRepository, Request $request, Security $security): Response
    {
        $user = $security->getUser();
        
        //on définit le nombre d'éléments par page
        $limit = 4;

        //on récupère le numéro de la page
        $page = (int)$request->query->get('page', 1);

              
        $posts = $blogpostsRepository->findPostByPageAndUser($user, $page, $limit);
       
        $total = $blogpostsRepository->getTotalPosts();
        
        return $this->render('blog/index.html.twig', [
            'posts' => $posts,
            'total' => $total,
            'limit' => $limit,
            'page' => $page,
        ]);
    }

    #[Route('/new', name: 'blog_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_AUTHOR')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $blogpost = new Blogposts();
        $form = $this->createForm(BlogpostsType::class, $blogpost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $blogpost->setAuteur($user);            
            $blogpost->setDatePost(new \DateTime());

            $entityManager->persist($blogpost);
            $entityManager->flush();

            return $this->redirectToRoute('blog_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('blog/new.html.twig', [
            'blogpost' => $blogpost,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'blog_show', methods: ['GET'])]
    #[IsGranted('ROLE_AUTHOR')]
    public function show(Blogposts $blogpost): Response
    {
        return $this->render('blog/show.html.twig', [
            'blogpost' => $blogpost,
        ]);
    }

    #[Route('/{id}/edit', name: 'blog_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_EDITOR')]
    public function edit(Request $request, Blogposts $blogpost, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BlogpostsType::class, $blogpost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('blog_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('blog/edit.html.twig', [
            'blogpost' => $blogpost,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'blog_delete', methods: ['POST'])]
    #[IsGranted('ROLE_AUTHOR')]
    public function delete(Request $request, Blogposts $blogpost, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blogpost->getId(), $request->request->get('_token'))) {
            $entityManager->remove($blogpost);
            $entityManager->flush();
        }

        return $this->redirectToRoute('blog_index', [], Response::HTTP_SEE_OTHER);
    }
}