<?php

namespace App\Controller\Admin;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    
    private $security;  

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/admin', name: 'admin')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    #[Route('/admin/profil', name: 'admin_profil')]
    #[IsGranted('ROLE_ADMIN')]
    public function profilAdmin(): Response
    {
        $user = $this->security->getUser();
        return $this->render('admin/profil-admin.html.twig', compact('user'));
    }
}