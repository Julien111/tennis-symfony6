<?php

namespace App\Controller\Home;

use App\Entity\Blogposts;
use App\Repository\BlogpostsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/blog', name: 'app_home_blog')]
    public function indexBlog(BlogpostsRepository $blogRepository, Request $request): Response
    {   
        //on définit le nombre d'éléments par page
        $limit = 4;

        //on récupère le numéro de la page
        $page = (int)$request->query->get('page', 1);
       
        $posts = $blogRepository->findPostByPage($page, $limit);

        $total = $blogRepository->getTotalPosts();

        
        return $this->render('home/blogIndex.html.twig', [
            'posts' => $posts,
            'total' => $total,
            'limit' => $limit,
            'page' => $page,

        ]);
    }

    #[Route('/blog/{id}', name: 'app_show_blog', methods: ['GET'])]
    public function show(Blogposts $blogpost): Response
    {
        return $this->render('home/show.html.twig', [
            'post' => $blogpost,
        ]);
    }

    #[Route('/about', name: 'app_about', methods: ['GET'])]
    public function aboutPage(): Response
    {
        return $this->render('home/about.html.twig', []);
    }
    
}