<?php

namespace App\Tests;

use App\Entity\Tournoi;
use PHPUnit\Framework\TestCase;

class TournoiTest extends TestCase
{
    public function testEntityValue(): void
    {
        $this->assertTrue(true);
        $tournoi = new Tournoi();
        $nomTournoi = "Nom tournoi";
        $tournoi->setNom($nomTournoi);
        $codeTournoi = "CODE";
        $tournoi->setCode($codeTournoi);
        
        $this->assertEquals("Nom tournoi", $tournoi->getNom());    
        $this->assertEquals("CODE", $tournoi->getCode());    
    }
}